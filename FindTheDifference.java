class Solution {
    public char findTheDifference(String s, String t) {
          if (s.length() == 0) {
            return t.toCharArray()[0];
        }
        char[] ss = s.toCharArray();
        char[] ts = t.toCharArray();
        int counts = 0;
        int countt = 0;
        for (int i = 0; i < ss.length; i++) {
            counts += ss[i];
            countt += ts[i];
        }
        countt += ts[ts.length - 1];

        return (char) (countt - counts);
    }
        
    }



