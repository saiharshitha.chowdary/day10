class Solution {
    public String replaceDigits(String s) {
           StringBuilder res = new StringBuilder();
    
    for(int i=0; i<s.length()-1; i+=2){
        
        char c = s.charAt(i);              
        int num = s.charAt(i+1)-'0';   
        
        res.append(c);                 
        res.append((char)(c+num)); 
        
    }
    
    if(s.length()%2!=0)                  
    res.append(s.charAt(s.length()-1));
    
    return res.toString();
    
}

        
    }
