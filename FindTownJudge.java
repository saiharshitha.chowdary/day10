class Solution {
    public int findJudge(int n, int[][] trust) {
        int cand = 0;
        int[] j = new int[n];
        for (int[] i: trust) {
            int a = i[0] - 1;
            int b = i[1] - 1;
            j[a] = -1; // not judge, trusts someone
            if (j[b] != -1) { 
                cand = b;
                j[b] += 1;
            }
                
        }
        if (j[cand] == n-1) return cand + 1;
        else return -1;
    }
}
        


        