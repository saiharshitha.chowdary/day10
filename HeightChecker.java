class Solution {
    public int heightChecker(int[] heights) {
          int arr[] = new int[100];
        for(int ele: heights) arr[ele-1]+=1;
        int i=0;
        int j=0;
        int cnt=0;
        while(i<100 && j<heights.length) {
            if(arr[i]>0) {
                arr[i]-=1;
                if(heights[j]!=i+1) cnt+=1;
                j+=1;
            } else {
                i++;
            }
        }
        return cnt;
    }
}
    
